#include <bits/stdc++.h>
using namespace std;
const int N= 3e5+5;
char str[N];
long long dp[4];
int main()
{
    scanf("%s", str + 1);
    str[0] = '0';
    long long sum = 0;
    for(int i = 1; str[i] ; i ++ ) {
        int dig = str[i] - '0' + 10 * (str[i-1] -'0');
        if(dig % 4 == 0 ) {
            sum += (i-1);
        }
        if((str[i]-'0') % 4 == 0) sum ++;
    }
    printf("%lld\n",sum);
}